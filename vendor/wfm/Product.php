<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Product
 *
 * @author User
 */
namespace wfm;

abstract class Product
{
    
    private $name;
    protected $price;
    private $discount = 0;
    
   /** public $public = 'PUBLIC';
    protected $protected = 'PROTECTED';
    private $private = 'PRIVATE'
    */
    
    public function __construct($name, $price) {
        $this->name = $name;
        $this->price = $price;
    }


    public function getProduct()
    {
        return "<hr><b>О товаре</b><br>"
                . "Наименование: {$this->name}<br>"
                . "Цена со скидкой {$this->getPrice()}<br>";
              
    }
    
    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price-($this->discount/100 * $this->price);
    }

    public function setDiscount($discount): void {
        $this->discount = $discount;
    }
    
    public function getDiscount() {
        return $this->discount;
    }


}
