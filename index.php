<?php

use wfm\interfaces\iGadget;
use app\{BookProduct, NotebookProduct};

require_once __DIR__ . '/vendor/autoload.php';
/*function autoloader($class)
{
    $class = str_replace("\\", '/', $class);
    echo $class."<br>";
    $file = __DIR__ . "/{$class}.php";
    if(file_exists($file)){
        require_once $file;
    }
}

spl_autoload_register('autoloader');*/

function debug($data){
    echo '<pre>' . print_r($data,1). '</pre>';
}   

function offerCase(iGadget $product){
    echo "<p>Предлагаем чехол для гаджета {$product->getName()}</p>";
}
$p1=new NotebookProduct('Dell', 1000, 'AMD');
$p2=new BookProduct('Три мушкетёра',20,180);
offerCase($p1);
debug($p2);

$mail = new \PHPMailer\PHPMailer\PHPMailer;

debug($mail);
