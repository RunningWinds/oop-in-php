<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of NotebookProduct
 *
 * @author User
 */
namespace app;

use wfm\interfaces\iGadget;
use wfm\Product;

class NotebookProduct extends Product implements iGadget{
    public $cpu;
    
    public function __construct($name, $price, $cpu) {
        parent::__construct($name, $price);
        $this->cpu = $cpu;
    }

    public function getProduct(){
        $out=parent::getProduct();
        $out.= "Цена без скидки: {$this->price}<br>"; 
        $out.= "Процессор: {$this->cpu}<br>";  
        $out.= "Cкидка: {$this->getDiscount()}%<br>";  
        return $out;
    }
    
    public function getCpu() {
        return $this->cpu;
    }

    public function getCase() {
        
    }

}
