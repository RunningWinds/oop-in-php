<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of BookProduct
 *
 * @author User
 */
namespace app;

use wfm\interfaces\I3D;
use wfm\Product;

class BookProduct extends Product implements I3D {
    public $numPages;
    
    public function __construct($name, $price, $numPages) {
        parent::__construct($name, $price);
        $this->numPages = $numPages;
    }

    public function test(){
        echo self::TEST2;
    }
    
    public function getProduct(){
        $out=parent::getProduct();
        $out.= "Цена без скидки: {$this->price}<br>";  
        $out.= "Количество страниц: {$this->numPages}<br>";  
        $out.= "Cкидка: {$this->getDiscount()}%<br>";  
        return $out;
    }
    
    public function getNumPages() {
        return $this->numPages;
    }
}
